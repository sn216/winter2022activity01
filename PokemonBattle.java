/* this programs simulate a battle between 2 pokemons teams
* written by : N. Sriraam # 2245165
* December 9th 2022
*/
import java.util.Scanner;
import java.util.Random;

public class PokemonBattle 
{

  public static final Scanner scan = new Scanner(System.in);
  
  /*
   * DO NOT MODIFY ANYTHING BETWEEN THESE COMMENTS
   */
  
  enum PokemonType {
    NORMAL, FIRE, WATER, ELECTRIC, GRASS, ICE, FIGHTING, POISON, GROUND, FLYING, PSYCHIC, BUG, ROCK, GHOST, DRAGON,
    DARK, STEEL, FAIRY
  }

  public static double ratioBetween(String pokemon1, String pokemon2) {
    int pokemonAttack = PokemonType.valueOf(pokemon1.toUpperCase()).ordinal();
    int pokemonDefense = PokemonType.valueOf(pokemon2.toUpperCase()).ordinal();
    double[][] attackRatio = {
                 // nor  fir  wat  ele  gra  ice  fig  poi  gro  fly  psy  bug  roc  gho  dra  dar  ste  fai
        /* nor */ { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 0.0, 1.0, 1.0, 0.5, 1.0 },
        /* fir */ { 1.0, 0.5, 0.5, 1.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 0.5, 1.0, 2.0, 1.0 },
        /* wat */ { 1.0, 2.0, 0.5, 1.0, 0.5, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 1.0, 1.0 },
        /* ele */ { 1.0, 1.0, 2.0, 0.5, 0.5, 1.0, 1.0, 1.0, 0.0, 2.0, 1.0, 1.0, 1.0, 1.0, 0.5, 1.0, 1.0, 1.0 },
        /* gra */ { 1.0, 0.5, 2.0, 1.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 1.0, 0.5, 2.0, 1.0, 0.5, 1.0, 0.5, 1.0 },
        /* ice */ { 1.0, 0.5, 0.5, 1.0, 2.0, 0.5, 1.0, 1.0, 2.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0 },
        /* fig */ { 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 0.5, 0.5, 0.5, 2.0, 0.0, 1.0, 2.0, 2.0, 0.5 },
        /* poi */ { 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 0.5, 0.5, 1.0, 1.0, 1.0, 0.5, 0.5, 1.0, 1.0, 0.0, 2.0 },
        /* gro */ { 1.0, 2.0, 1.0, 2.0, 0.5, 1.0, 1.0, 2.0, 1.0, 0.0, 1.0, 0.5, 2.0, 1.0, 1.0, 1.0, 2.0, 1.0 },
        /* fly */ { 1.0, 1.0, 1.0, 0.5, 2.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 1.0, 1.0, 0.5, 1.0 },
        /* psy */ { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 1.0, 1.0, 0.5, 1.0, 1.0, 1.0, 1.0, 0.0, 0.5, 1.0 },
        /* bug */ { 1.0, 0.5, 1.0, 1.0, 2.0, 1.0, 0.5, 0.5, 1.0, 0.5, 2.0, 1.0, 1.0, 0.5, 1.0, 2.0, 0.5, 0.5 },
        /* roc */ { 1.0, 2.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 0.5, 2.0, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 0.5, 1.0 },
        /* gho */ { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 1.0 },
        /* dra */ { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 0.5, 0.0 },
        /* dar */ { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.5, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 2.0, 1.0, 0.5, 1.0, 0.5 },
        /* ste */ { 1.0, 0.5, 0.5, 0.5, 1.0, 2.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0, 0.5, 2.0 },
        /* fai */ { 1.0, 0.5, 1.0, 1.0, 1.0, 1.0, 2.0, 0.5, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 0.5, 1.0 } };
    return attackRatio[pokemonAttack][pokemonDefense];
  }

  /*
   * WRITE YOUR SOLUTION BELOW :)
   */
  
  // your methods go here
  
  public static double[] typeAttackRatio (String [] team1, String [] team2)
  {
    double [] typeRatios = new double [team1.length];

    for(int i =0; i < team1.length; i++)
    {
      typeRatios[i] = ratioBetween(team1[i], team2[i]);
    }
    return typeRatios;
  }

  public static int[] levelAttackRatio (int [] levelsTeam1, int [] levelsTeam2)
  {
    int [] attackRatio = new int [levelsTeam1.length];

    for(int i=0; i < levelsTeam1.length; i++)
    {
      if((levelsTeam1[i] - levelsTeam2[i]) <=  -10)
      {
        attackRatio[i] = 1;
      }
      else if((levelsTeam1[i] - levelsTeam2[i]) <= 9 && (levelsTeam1[i] - levelsTeam2[i]) >= -9)
      {
        attackRatio[i] = 10;
      }
      else
      {
        attackRatio[i] = (levelsTeam1[i] - levelsTeam2[i]) + 10;
      }
    }
    return attackRatio;
  }

  public static double[] calculateHP (int [] levelsTeam1, int [] levelsTeam2)
  {
    double [] HPsTeam1 = new double [levelsTeam1.length];

    for(int i=0; i < levelsTeam1.length; i++)
    {
      HPsTeam1[i] = levelsTeam1[i] * 10.0;
    }
    return HPsTeam1;
  }

  public static String[] collectPokemonNames()
  {
    System.out.println("Please enter the size of your Pokemon team:");
    int sizeTeam = scan.nextInt();
    String [] pokemonNamesList = new String [sizeTeam];

    System.out.println("Please enter the Pokemons for the team");

    for(int i=0; i < sizeTeam; i++)
    {
      pokemonNamesList[i] = scan.next();
    }
    return pokemonNamesList;
  }
  
  public static String[] collectPokemonTypes(int amountPokemons)
  {
    

    String [] pokemonTypesList = new String [amountPokemons];

    System.out.println("Please enter the corresponding Pokemon types for the team:");

    for(int i=0; i < amountPokemons; i++)
    {
      pokemonTypesList[i] = scan.next();
    }
    return pokemonTypesList;
  }

  public static int[] collectPokemonLevels(int amountPokemons)
  {
  

    int [] pokemonLevelsList = new int [amountPokemons];

    System.out.println("Please enter the Pokemons levels for the team:");

    for(int i=0; i < amountPokemons; i++)
    {
      pokemonLevelsList[i] = scan.nextInt();
    }
    return pokemonLevelsList;
  }

  public static double damageFromAttackMove(int levelAttackRatio,double typeAttackRatio,double oppInitialHP, double oppCurrentHP)   
  {
    Random rand = new Random();

    double attackDamage = 0.0;
    int criticalHit = rand.nextInt(100) + 1;
    if(criticalHit == 42)
    {
      System.out.println("It's a crit!!");
      attackDamage = oppCurrentHP;
    }
    else
    {
      attackDamage = ((levelAttackRatio / 100.0) * oppInitialHP) * typeAttackRatio;
    }
    return attackDamage;
  }

  public static void determineWinningTeam(double HPTeam1[],double HPTeam2[])
  {
    int alivePokemonTeam1 = 0;
    int alivePokemonTeam2 = 0;

    for(int i=0;i < HPTeam1.length;i++)
    {
      if(HPTeam1[i] > 0)
      {
        alivePokemonTeam1++;
      }
    }

    for(int i=0;i < HPTeam2.length;i++)
    {
      if(HPTeam2[i] > 0)
      {
        alivePokemonTeam2++;
      }
    }

    if(alivePokemonTeam1 > alivePokemonTeam2)
    {
      System.out.println("Team 1 wins the battle!");
      System.out.println("Because Team 1 has " + alivePokemonTeam1 + " victories over only " + alivePokemonTeam2 + " victories of Team 2");
    }

    else if(alivePokemonTeam2 > alivePokemonTeam1)
    {
      System.out.println("Team 2 wins the battle!");
      System.out.println("Because Team 2 has " + alivePokemonTeam2 + " victories over only " + alivePokemonTeam1 + " victories of Team 1");
    }

    else
    {
      System.out.println("It's a tie!");
      System.out.println("Because both teams has the same amount of victories");
    }
  }
  public static void main(String[]args)
  {
    Random rand = new Random();

    /*invoking the methods to assign the name, type, and levels of the pokemon's in Team1
      the variable amountPokemons is used to the second team; we assume the amount of pokemon is the same for both teams
    */
    String[] pokemonTeam1 = collectPokemonNames();
    int amountPokemons= pokemonTeam1.length;
    String[] pokemonType1 = collectPokemonTypes(amountPokemons);
    int[] pokemonLevelsTeam1 = collectPokemonLevels(amountPokemons);
    
    //invoking the methods to assign the name, type, and levels of the pokemon's in Team2
    String[] pokemonTeam2 = collectPokemonNames();
    String[] pokemonType2 = collectPokemonTypes(amountPokemons);
    int[] pokemonLevelsTeam2 = collectPokemonLevels(amountPokemons);
    
    //array to store team2 and team2 individual pokemon's health point and determine how many pokemon have 0HP at the end of all rounds
    double [] calculateHpTeam1 = calculateHP (pokemonLevelsTeam1, pokemonLevelsTeam2);
    double [] calculateHpTeam2 = calculateHP (pokemonLevelsTeam2, pokemonLevelsTeam1);

    //array to store the type attack ratio level attack ratio if the first team starts the round
    double [] typeAttackRatioTeam1 = typeAttackRatio (pokemonType1,pokemonType2);
    int [] levelAttackRatioTeam1 = levelAttackRatio (pokemonLevelsTeam1, pokemonLevelsTeam2);

    
    //array to store the type attack ratio and level attack ratio if the second team starts the round
    double [] typeAttackRatioTeam2 = typeAttackRatio (pokemonType2,pokemonType1);
    int [] levelAttackRatioTeam2 = levelAttackRatio (pokemonLevelsTeam2, pokemonLevelsTeam1);
   

    //loops for rounds
    for(int i=0; i < pokemonTeam1.length; i++)
    {
      //current and initial hp of team1
      double initialHP1 = calculateHpTeam1[i];
      double currentHP1 = calculateHpTeam1[i];

      //current and initial hp of team2
      double initialHP2 = calculateHpTeam2[i];
      double currentHP2 = calculateHpTeam2[i];
      
      //initializes the info of both team's pokemon and the number of round 
      System.out.println("Round " + (i + 1) + " starts: " + pokemonTeam1[i] + ", " + pokemonType1[i] + " level " + pokemonLevelsTeam1[i] + " ," + " HP: " + calculateHpTeam1[i] + " vs ");
      System.out.print(pokemonTeam2[i] + ", " + pokemonType2[i] + " level " + pokemonLevelsTeam2[i] + " ," + " HP: " + calculateHpTeam2[i]);
      System.out.println();

      //generates a number between 1 and 2, if it's 1, team 1 will start, but if it's two, the second team will start the round
      int roundTeamRandomizer = rand.nextInt(2) +1 ;
      
      //team 1 starts the round
      if (roundTeamRandomizer == 1)
      {
        System.out.println(pokemonTeam1[i] + " starts the battle!");
        
        while(currentHP1 > 0 && currentHP2 > 0)
        { 
          //the pokemon in team1 attacks the pokemon in team2
          double attackDamage1 = damageFromAttackMove(levelAttackRatioTeam1[i], typeAttackRatioTeam1[i], initialHP2, currentHP2);
          currentHP2 = currentHP2 - attackDamage1;

          System.out.println(pokemonTeam1[i] + " attacks and causes " + attackDamage1 + " HP damage on " + pokemonTeam2[i] + ". " + pokemonTeam2[i] + " HP is now " + currentHP2 );
          
          //changes array calculateHP to 0 for the determineWinningTeam method and checks if the first pokemon is dead so the second pokemon does not attack
          if(currentHP2 <= 0)
          {
            System.out.println(pokemonTeam2[i] + " has fainted.");
            calculateHpTeam2[i] = 0;
            break;
          }

          else
          {
            //the pokemon in team2 attacks the pokemon in team1 after receiving the attack from team1
            double attackDamage2 = damageFromAttackMove(levelAttackRatioTeam2[i], typeAttackRatioTeam2[i], initialHP1, currentHP1);
            currentHP1 = currentHP1 - attackDamage2;

            System.out.println(pokemonTeam2[i] + " attacks afterwards and causes " + attackDamage2 + " HP damage on " + pokemonTeam1[i] + ". " + pokemonTeam1[i] + " HP is now " + currentHP1 );
            
            //changes array calculateHP to 0 for the determineWinningTeam method
            if(currentHP1 <= 0)
            {
              System.out.println(pokemonTeam1[i] + " has fainted.");
              calculateHpTeam1[i] = 0;
            }
          }
        }
      }

      //Team 2 starts the round
      else
      {
        System.out.println(pokemonTeam2[i] + " starts the battle!");

        while(currentHP1 > 0 && currentHP2 > 0)
        { 
          //the pokemon in team2 attacks the pokemon in team1
          double attackDamage1 = damageFromAttackMove(levelAttackRatioTeam2[i], typeAttackRatioTeam2[i], initialHP1, currentHP1);
          currentHP1 = currentHP1 - attackDamage1;

          System.out.println(pokemonTeam2[i] + " attacks and causes " + attackDamage1 + " HP damage on " + pokemonTeam1[i] + ". " + pokemonTeam1[i] + " HP is now " + currentHP1);
          
          //changes array calculateHP to 0 for the determineWinningTeam method and checks if the first pokemon is dead so the second pokemon does not attack
          if(currentHP1 <= 0)
          {
            System.out.println(pokemonTeam1[i] + " has fainted");
            calculateHpTeam1[i] = 0;
            break;
          }
           
          else
          {
            //the pokemon in team1 attacks the pokemon in team2 after receiving the attack from team2
            double attackDamage2 = damageFromAttackMove(levelAttackRatioTeam1[i], typeAttackRatioTeam1[i], initialHP2, currentHP2);
            currentHP2 = currentHP2 - attackDamage2;

            System.out.println(pokemonTeam1[i] + " attacks afterwards and causes " + attackDamage2 + " HP damage on " + pokemonTeam2[i] + ". " + pokemonTeam2[i] + " HP is now " + currentHP2);

            //changes array calculateHP to 0 for the determineWinningTeam method
            if(currentHP2 <=0)
            {
              System.out.println(pokemonTeam2[i] + " has fainted");
              calculateHpTeam2[i] = 0;
            }
          } 
        }
      }
      System.out.println();
    }
    //invokes the methods to determine who won the fight after looping all rounds
    determineWinningTeam(calculateHpTeam1, calculateHpTeam2);
    scan.close();
  }
}


  
  
